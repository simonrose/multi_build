# Multi-stage builds in E3

This repository presents a mechanism to build an E3 module in multiple stages. The main use case
is an EPICS module that contains proprietary materials that may not be able to be shared; `opcua`
is one such module, where the `opcua` module cannot be built without the United Automation header
files. However, these are not allowed to be distributed beyond a license holder.

## Idea

The idea is as follows: Once the module is built (but not installed), it populates the directories
`O.$(BASEVERSION)_Common` and `O.$(BASEVERSION)_$(T_A)`. Running `make install` will simply check
if these directories are newer than their dependencies; if they are, then it will not need to run
`make build` again. As such we can simply store the contents of these directories and then put
them back in the correct place.

## Usage

The usage is very easy: as the person with the license, you just need to run `make stash`, which 
will:

1. Build the module
2. Create a branch based off of the current location in the git tree, named in a unique way based
   on base version, require version, and a few other parameters
3. Copy the contents of `O.$(BASEVERSION)` directories to the wrapper
4. Commit those changes

At that point, the builder should push these changes to the remote repository so that another
developer can install the module.

The second developer would check out the branch created above and do the following:

1. Run `make unstash` which copies the stashed folders back to the module directory
2. Run `make install`. Since the stashed data is all newer than its dependencies, the build
   process will not be re-run and the module is installed.

## Changes

There are only a few small changes, which could be added to the `require` module if this is deemed
a reasonable addition.

### RULES_MODULE

This contains the new rules `stash` and `unstash`.

### stash_library.sh

This is a script which simply copies the generated `O.$(BASEVERSION)` directories to a `stash` 
directory, which it then commits onto a new branch.

## Issues

After experimenting with the module that inspired all of this, here are a few other issues that
have arisen.

* The source files that depend on the vendor headers (which are not to be distributed) end up
  still having that dependency in the `.d` files, so those need to be removed. This can be done
  via
  ```bash
  grep -r "uasdk/include" --include *.d stash -l | xargs sed -i '/uasdk\/include\/.*\.h/d'
  ```
  or something similar.
* The stashing process would also need to stash and unstash the vendor libraries; this is a little
  tricker to automate, as the vendor library paths are hard-coded, and the make process actually
  checks which libraries are there. So that would need to change.

## Conclusion

This is probably no cleaner than the previous method which uses two repositories; at best we may
need a module makefile which has a forked path, or a manual process to deal with the vendor
libraries. While this was an interesting thing to try out, I don't think that I can say it is any
better than the other method.