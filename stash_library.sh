#!/bin/env bash

while getopts "m:b:r:v:u" opt; do
    case $opt in
    m)
        module=$OPTARG
        ;;
    b)
        base_ver=$OPTARG
        ;;
    r)
        req_ver=$OPTARG
        ;;
    v)
        e3_ver=$OPTARG
        ;;
    esac
done

if [ ! -d "$module" ]; then
    echo "Invalid directory to stash" >&2
    exit 1
fi

git_commit=$(cd $module && git rev-parse --verify --short HEAD)
datestamp=$(date +%Y%m%d)
timestamp=$(date +%H%M%S)

generate_tag() {
    echo "$base_ver-$req_ver/$e3_ver-$git_commit-$datestamp"T"$timestamp"
}

create_description() {
    echo "EPICS version:     $base_ver" >stash/stash_description.txt
    echo "Require version:   $req_ver" >>stash/stash_description.txt
    echo "E3 Module version: $e3_ver" >>stash/stash_description.txt
    echo "EPICS module hash: $git_commit" >>stash/stash_description.txt
    echo "Timestamp:         ${datestamp}T${timestamp}" >>stash/stash_description.txt
}

tag="$(generate_tag)"
git checkout -b "$tag"

mkdir stash

create_description

for d in $module/O.*; do
    build_dir="$(basename $d)"
    cp -r "$d" stash
    # In case O.$BASE_VERSION_Common is empty...
    touch "stash/$build_dir/.keep"
done

git add stash/
git commit -m "Stashed E3 module $module with tag $tag"

echo "# -------------------------------------------------------------------------------"
echo "# Note: You are currently on branch $tag."
echo "# To push the stash to the remote:"
echo "#     git push [origin] $tag"
echo "#"
echo "# To return to the previous branch:"
echo "#     git checkout -"
echo "# -------------------------------------------------------------------------------"
